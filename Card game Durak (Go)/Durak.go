package main

import (
	"errors"
	"fmt"
	"os"
	"os/exec"
	"time"
)

var cards = map[int]int{1: 61, 2: 62, 3: 63, 4: 64, 5: 71, 6: 72, 7: 73, 8: 74, 9: 81, 10: 82, 11: 83, 12: 84, 13: 91, 14: 92, 15: 93, 16: 94, 17: 101, 18: 102,
	19: 103, 20: 104, 21: 111, 22: 112, 23: 113, 24: 114, 25: 121, 26: 122, 27: 123, 28: 124, 29: 131, 30: 132, 31: 133, 32: 134, 33: 141, 34: 142, 35: 143, 36: 144}

func scanerr(i int, a []int) error {
	inx := 0
	for in, v := range a {
		if v == 0 {
			inx = in
			break
		}
	}
	if i > inx+1 || i < 0 {
		return errors.New("son xato")
	}
	return nil
}

type cardGame struct {
	card      map[int]int
	user      []int
	kamp      []int
	userFront []int
	kampFront []int
	trump     int
	a         int
	b         bool
}

func clear() {
	cmd := exec.Command("clear")
	cmd.Stdout = os.Stdout
	cmd.Run()
}
func (c *cardGame) sort() {
	var newArr1 []int
	var newArr2 []int
	for i := 6; i < 15; i++ {
		for j := 0; j < len(c.kamp); j++ {
			if i == c.kamp[j]/10 && c.trump%10 != c.kamp[j]%10 {
				newArr1 = append(newArr1, c.kamp[j])
			}
		}
		for j := 0; j < len(c.user); j++ {
			if i == c.user[j]/10 && c.trump%10 != c.user[j]%10 {
				newArr2 = append(newArr2, c.user[j])
			}
		}
	}
	for i := 6; i < 15; i++ {
		for j := 0; j < len(c.kamp); j++ {
			if i == c.kamp[j]/10 && c.trump%10 == c.kamp[j]%10 {
				newArr1 = append(newArr1, c.kamp[j])
			}
		}
		for j := 0; j < len(c.user); j++ {
			if i == c.user[j]/10 && c.trump%10 == c.user[j]%10 {
				newArr2 = append(newArr2, c.user[j])
			}
		}
	}
	for i := len(newArr1); i < 36; i++ {
		newArr1 = append(newArr1, 0)
	}
	for i := len(newArr2); i < 36; i++ {
		newArr2 = append(newArr2, 0)
	}
	c.kamp = newArr1
	c.user = newArr2
}
func (c *cardGame) giveUser() {
	ind := 5
	for kay, val := range c.card {
		if c.user[ind] == 0 {
			c.user[ind] = val
			delete(c.card, kay)
		} else {
			break
		}
		if ind != 0 {
			ind--
		}
	}
	if len(c.card) == 0 && c.b {
		if c.kamp[ind] == 0 {
			c.kamp[ind] = c.trump
			c.b = false
		}
	}
}
func (c *cardGame) giveKamp() {
	ind := 5
	for kay, val := range c.card {
		if c.kamp[ind] == 0 {
			c.kamp[ind] = val
			delete(c.card, kay)
		} else {
			break
		}
		if ind != 0 {
			ind--
		}
	}
	if len(c.card) == 0 && c.b {
		if c.kamp[ind] == 0 {
			c.kamp[ind] = c.trump
			c.b = false
		}
	}

}
func (c *cardGame) newGame() {
	c.b = true
	c.card = cards
	c.kamp = make([]int, 36)
	c.user = make([]int, 36)
	c.kampFront = make([]int, 6)
	c.userFront = make([]int, 6)
	for key, i := range c.card {
		c.trump = i
		delete(c.card, key)
		break
	}
}
func front1(a int) string {
	str := "    "
	if a == 0 {
		return str
	} else if 59 < a {
		switch a / 10 {
		case 6:
			str = " 6"
		case 7:
			str = " 7"
		case 8:
			str = " 8"
		case 9:
			str = " 9"
		case 10:
			str = "10"
		case 11:
			str = " J"
		case 12:
			str = " Q"
		case 13:
			str = " K"
		case 14:
			str = " T"
		}
		switch a % 10 {
		case 1:
			str += "♡️"
		case 2:
			str += "♢️"
		case 3:
			str += "♠️"
		case 4:
			str += "♣️"
		}
		return str
	}
	return "   "
}
func front2(a int) string {
	if a > 0 {
		return " 🃏 "
	}
	return "    "
}
func (c *cardGame) printCard() {
	c.sort()
	var k1, k7, k8, k9, k10, k11, k12, k13, k14, k15, k16, k17, k18, k19, k20 string
	clear()
	l := 0
	if len(c.card) == 0 {
		l = -1
	} else {
		l = len(c.card)
	}
	k7 = front2(len(c.card))
	k8 = front1(c.kampFront[0])
	k9 = front1(c.kampFront[1])
	k10 = front1(c.kampFront[2])
	k11 = front1(c.kampFront[3])
	k12 = front1(c.kampFront[4])
	k13 = front1(c.kampFront[5])
	if len(c.card) != 0 {
		k14 = front1(c.trump)
	} else {
		k14 = "    "
	}
	k15 = front1(c.userFront[0])
	k16 = front1(c.userFront[1])
	k17 = front1(c.userFront[2])
	k18 = front1(c.userFront[3])
	k19 = front1(c.userFront[4])
	k20 = front1(c.userFront[5])
	if c.a == 1 {
		k1 = "👇"
	} else {
		k1 = "👆"
	}
	fmt.Print("          |")
	for _, i := range c.kamp {
		if i != 0 {
			fmt.Print(" 🃏 |")
		}
	}
	fmt.Println()
	fmt.Printf("\n%s%d %s |%s|%s|%s|%s|%s|%s|\n"+
		"|%s|    |%s|%s|%s|%s|%s|%s|\n", k7, l+1, k1, k8, k9, k10, k11, k12, k13, k14, k15, k16, k17, k18, k19, k20)
	fmt.Print("\n          |")
	for _, i := range c.user {
		if i != 0 {
			fmt.Print(front1(i), "|")
		} else {
			break
		}
	}
	fmt.Print("\n        ")
	for key, i := range c.user {
		if i != 0 {
			if key < 9 {
				fmt.Print("    ", key+1)
			} else {
				fmt.Print("   ", key+1)
			}
		} else {
			break
		}
	}
	fmt.Print("\nWhich card do you walk or pass [0](Qaysi kartani yurasiz yoki o'tish [0]): ")
}
func (c cardGame) check(i int) bool {
	for _, v := range c.kampFront {
		if v/10 == i/10 {
			return true
		}
	}
	for _, v := range c.userFront {
		if v/10 == i/10 {
			return true
		}
	}
	return false
}
func (c *cardGame) checkUser() bool {
	for _, i := range c.user {
		if i == 0 {
			return false
		}
		for _, j := range c.userFront {
			if i/10 == j/10 {
				return true
			}
		}
		for _, j := range c.kampFront {
			if i/10 == j/10 {
				return true
			}
		}
	}
	return false
}
func (c *cardGame) checkKamp() bool {
	for _, i := range c.kamp {
		if i == 0 {
			return false
		}
		for _, j := range c.userFront {
			if i/10 == j/10 {
				return true
			}
		}
		for _, j := range c.kampFront {
			if i/10 == j/10 {
				return true
			}
		}

	}
	return false
}
func checkCut(c1, c2, trump int) bool {
	if c1/10 < c2/10 && c1%10 == c2%10 || c2%10 == trump%10 {
		return true
	} else if c2%10 == trump%10 && c1%10 != trump%10 {
		return true
	}
	return false
}
func (c *cardGame) moveUser() bool {
	a := 0
son1:
	_, err := fmt.Scan(&a)
	if err != nil {
		fmt.Print("\niltimos son kirining: ")
		goto son1
	}
	errr := scanerr(a, c.user)
	if errr != nil {
		fmt.Print("\nto'ri son kiriting: ")
		goto son1
	}
	if a == 0 {
		return false
	} else {
		for k, v := range c.userFront {
			if v == 0 {
				if k == 0 {
					c.userFront[k] = c.user[a-1]
					c.user[a-1] = 0
				} else if c.check(c.user[a-1]) && c.kamp[0] != 0 {
					c.userFront[k] = c.user[a-1]
					c.user[a-1] = 0
					if k == 5 {
						return false
					}
				}
				return true
			}
		}
	}
	return true
}
func (c *cardGame) cutUser() bool {
	a := 0
son:
	_, err := fmt.Scan(&a)
	if err != nil {
		fmt.Print("\niltimos son kirining: ")
		goto son
	}
	errr := scanerr(a, c.user)
	if errr != nil {
		fmt.Print("\nto'ri son kiriting: ")
		goto son
	}
	if a == 0 {
		for k, i := range c.user {
			if i == 0 {
				for key, j := range c.kampFront {
					if j != 0 {
						c.user[k] = j
						c.kampFront[key] = 0
						k++
					} else {
						break
					}
				}
				for key, j := range c.userFront {
					if j != 0 {
						c.user[k] = j
						c.userFront[key] = 0
						k++
					} else {
						break
					}
				}
			}
		}
		return false
	} else {
		for k, i := range c.userFront {
			if i == 0 {
				if checkCut(c.kampFront[k], c.user[a-1], c.trump) {
					c.userFront[k] = c.user[a-1]
					c.user[a-1] = 0
					if k != 5 {
						if c.kampFront[k+1] == 0 {
							return false
						}
					} else {
						return false
					}
				}
			}
		}
	}
	return true
}
func (c *cardGame) moveKamp() {
	i := 0
	for k, v := range c.kampFront {
		if v == 0 {
			for i < 6 {
				if c.check(c.kamp[i]) && c.user[0] != 0 {
					c.kampFront[k] = c.kamp[i]
					c.kamp[i] = 0
					break
				} else if k == 0 {
					c.kampFront[k] = c.kamp[i]
					c.kamp[i] = 0
					break
				}
				i++
			}
		}
	}
}
func (c *cardGame) cutKamp() {
	for k, v := range c.userFront {
		if v != 0 && c.kampFront[k] == 0 {
			for key, i := range c.kamp {
				if checkCut(v, i, c.trump) {
					c.kampFront[k] = c.kamp[key]
					c.kamp[key] = 0
					break
				}
			}
			if c.kampFront[k] == 0 {
				for k, i := range c.kamp {
					if i == 0 {
						for key, j := range c.kampFront {
							if j != 0 {
								c.kamp[k] = j
								c.kampFront[key] = 0
								k++
							} else {
								break
							}
						}
						for key, j := range c.userFront {
							if j != 0 {
								c.kamp[k] = j
								c.userFront[key] = 0
								k++
							} else {
								break
							}
						}
					}
				}
			}
		} else if v == 0 {
			break
		}
	}
}
func (c cardGame) oneMove() bool {
	for i := 6; i < 15; i++ {
		for _, j := range c.kamp {
			if j%10 == c.trump%10 && j/10 == i {
				return false
			}
		}
		for _, j := range c.user {
			if j%10 == c.trump%10 && j/10 == i {
				return true
			}
		}
	}
	return true
}
func (c cardGame) finsh() bool {
	if len(c.card) == 0 && c.kamp[0] == 0 && c.user[0] == 0 {
		clear()
		fmt.Println("Durang!!!")
		return true
	} else if len(c.card) == 0 && c.kamp[0] == 0 {
		clear()
		fmt.Println("Yutqazdingiz!!!")
		return true
	} else if len(c.card) == 0 && c.user[0] == 0 {
		clear()
		fmt.Println("Yutdingiz!!!")
		return true
	}
	return false
}
func (c cardGame) same() bool {
	for k, v := range c.kampFront {
		if v == 0 {
			if c.userFront[k] == 0 {
				return true
			}
			return false
		}
	}
	return false
}
func (g *cardGame) play() {
	g.newGame()
	g.giveKamp()
	g.giveUser()
	if g.oneMove() {
		g.a = 1
	} else {
		g.a = 2
	}
	g.printCard()
	for {
		if g.a == 1 {
			g.giveKamp()
			g.giveUser()
			g.printCard()
			for {
				b := g.moveUser()
				if !b {
					g.cutKamp()
					if !g.same() {
						g.printCard()
						if !g.checkUser() {
							break
						}
					} else {
						g.printCard()
						break
					}
				} else {
					g.printCard()
					if !g.checkUser() {
						g.cutKamp()
						g.printCard()
						if !g.checkUser() {
							break
						}
					}
				}
			}
			time.Sleep(2 * time.Second)

		} else {
			g.giveUser()
			g.giveKamp()
			g.printCard()
			for {
				g.moveKamp()
				g.printCard()
				for {
					if !g.cutUser() {
						g.printCard()
						break
					}
					g.printCard()
				}
				if !g.checkKamp() {
					break
				}
			}
		}
		if g.userFront[0] != 0 && g.kampFront[0] != 0 {
			g.userFront = make([]int, 6)
			g.kampFront = make([]int, 6)
			if g.a == 2 {
				g.a = 1
			} else {
				g.a = 2
			}
		}
		g.sort()
		if g.finsh() {
			break
		}
	}
}
func main() {
	game := cardGame{}
	game.play()
}
